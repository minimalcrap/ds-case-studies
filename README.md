## Data Science Case Studies
The project contains different case studies for data science and statistical analysis using python and R
along with the code and examples.

### Case Study 1 (Web Log Analysis)

#### Description
A company in US has androind / ios applications along with the website which helps user maintain
their lawns. Due to different platform, backend had to handle login/signup separately for each
of the platform. The backend usually dumps the log on the server which are helpful for debugging.
The logs can be further categorized as info logs and debug logs.
Since, the user handling for different platform was getting out of hand, therefore they started
using a service called gigya, gigya is a user management platform which integrates with a wide
variety of cms platforms like drupal, magento etc and also provide different methods to allow
user management, whether they logged in through website, facebook , google or any other social 
platform. Gigya also got acquired by SAP for 350 million dollars.
Coming back to the scenario, earlier the US company (name not disclosed due to privacy reasons),
handled users by itself, but after gigya integration in their system, they didn't want to discard
the old users right away since they still had their login through their website , so they kept their
old login handling live and logged the users under info category for every login, thus tracking if 
the user logged in from gigya or their website.
Meanwhile rolling down the new apps and website with gigya integration, they faced substancial amount of
errors which went into the logs.
Now, in a meeting regarding this transition, they discussed following issues in question:

- what sort of errors are occuring more frequently.
- Meanwhile the debugging is going on by backend developers, what is the rate of occurence of errors per day.
- How many users login through old login method per day.
- How many users login through gigya method per day.
- What is the average rate at which the users are migrating from old login to gigya.

#### Data Availability.
- info logs plain text file of past 10 days.
- error logs plain text file of past 10 days.

#### Problem Statements:
- What is the rate (number of errors per day) of errors occurence for the 10 day duration.
- How many users login through old login method per day.
- How many users login through gigya method per day.
- What is the average rate (for 10 day duration) at which users are migration old login to gigya.


### Case Study 2 (Product Sale Predictive analysis)

#### Description

A company in India, gurgaon recently setup its B2C (Businenss to customer) e-commerce website to
sell different sort of pantry items. User go to their website and order different pantry items
based on the item availability. If a particular item exists then it is sent to the user via courier,
if it is not available, then user may choose to either not buy or put a backorder.
To be able to successfuly cater the needs of the customer, company needs ensure the item is available
in the inventory, otherwise if the item is frequently unavailable, the customer might loose interest
and the website may loose the customers.
Also company needs to ensure the sale of different products, so they wanna show suggestions to existings
users regarding items based on their purchase history.

#### Data Availability.
A mysql database backup file containing following tables data for a duration of 1 month:
- product's category master.
- product's subcategory master.
- item's master table with category and sub category details.
- table containing customers transactions.
- table containing customers backorders.
- table containing data for item visited but not bought by customers.
- table containing inventory stock detail for various items.

#### Problem Statemens:
- For a certain item, depict its sale graph through a month.
- For a certain user, show through pie chart or a relevant graph the percentage of items he bought from a particular category.
- For a certain user, predict the items he/she would like to buy based on their purchase history.
- For a certain item, predict the exact quantity of items to be retained for next month.


### Case Study 3 (Online cooking video tutorial website)

#### Description

Allan harper from United State, Texas had a youtube channel about cooking videos. The channel gained quite
popularity across internet and people from different region of world started following his channel. He later
decides to launch a dedicated website for the same where he categorized different cooking video tutorials 
(categories like chinese, thai, italian etc) and create a user login system where user can register / login 
and watch videos. Allan's website also records the ip address of user accessing videos and determine the person's
location, thus capturing demographic data related to a particular user. To increase traffic on his website and
cater the user's what they would actually like, Allan wants to create some sort of predictive model and also
create a dashboard where he can view all sort reports like logins from a specific region, user's affinity towards
particular kind of videos etc.

#### Data Availability.
A mysql database containing following tables data for a duration of 1 month:
- demographic regions master.
- videos' category master.
- videos viewed by users (kind of an association table between user and videos).
- user's table containing all sort of demographic information along with personal information.
- videos table containing different sort of information like category, url, date of upload etc.


#### Problem Statements:
- based on provided data, visualize the categories and the percentage of views they got.
- based on a particular users view list, predict top 4 videos which he might like to watch.
- based on provided data, determine that which category may need more videos as per users likability.
- based on provided data, visualize the relation between demographic regions and the percentag of videos they
like from each video category.


### Case Study 4 (German bank customer loaning scenario)

#### Description
A german bank provides loan to its customer. Each customer have their account in the bank and have different sort of
attributes, on basis of which the bank evaluates if it can provide loan to a particular customer or not. In this case
scenario, we are going to assess the different attributes of the customers and calculate the risks for providing loan to
them.

#### Data Availability
- Dataset containing the different attributes for a particular customer.
- A text file which describes the attributes of customer since the dataset contains codes to simplify the database entry.

#### Problem Statement
- draw a chart showing the customers and their credit attribute.
- show in a piechart that how many customers are employed and unemployed.
- draw a model to calculate the risks while lending loan to a particular customer, therefore classify the customers based on their credits.